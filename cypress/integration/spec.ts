describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.navigate('/');
    cy.contains('Top Heroes');
    cy.contains('Hero Search');
    cy.task('log', 'This will be output to the terminal');
  });
});
