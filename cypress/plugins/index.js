const cypressTypeScriptPreprocessor = require("./cy-ts-preprocessor");
const registerCodeCoverageTasks = require('@cypress/code-coverage/task');

module.exports = (on, config) => {
  on('task', {
    log(message) {
      console.log(message);

      return null;
    },
  });

  on('file:preprocessor', cypressTypeScriptPreprocessor);
  registerCodeCoverageTasks(on, config);
  return config;
};
