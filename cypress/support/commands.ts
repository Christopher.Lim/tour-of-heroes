import '@cypress/code-coverage/support';

declare global {
  namespace Cypress {
    interface Chainable<Subject> {
      navigate(url: string): Chainable;
    }
    interface Window {
      console: any;
    }
  }
}

Cypress.Commands.add('navigate', (url) => {
  cy.log(`Navigating to ${url}`);
  return cy.visit('http://localhost:4200' + url);
});
